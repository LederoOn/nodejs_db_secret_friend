const db = require("../database/friends.json")
const controllers = require("../controllers/friendsController.js")

exports.newFriend = (req, res) => {
    const request = controllers.addNew(req.body)
    if (request === 400) { return res.status(400).json({"msg": "bad json arguments"})}
    return request ? res.status(201).json(request) : res.status(400).json({"msg": "email already exists"})
};

exports.getAll = (req, res) => {
    return res.status(200).json(controllers.getFriend())
};

exports.getById = (req, res) => {
    const request = controllers.getFriend(parseInt(req.query.id))
    return request ? res.status(200).json(request) : res.status(404).json({"msg": "friend not found"})
};

exports.updateById = (req, res) => {
    const request = controllers.updateFriend(parseInt(req.query.id), req.body)
    if (request === 400) { return res.status(400).json({"msg": "email already exists"})}
    return request ? res.status(200).json(request) : res.status(404).json({"msg": "friend not found"})
};

exports.deleteById = (req, res) => {
    const request = controllers.deleteFriend(parseInt(req.query.id))
    if (request === 400) { return res.status(400).json({"msg": "friend not found"})}
    return request ? res.status(200).json(request) : res.status(404).json({"msg": "friend not found"})
};

exports.sendMailById = async (req, res) => {
    console.log(req.body)
    const request = await controllers.sendMailById(req.body)
    return res.json({"msg": "Secret Friend choosed!", "link": request})
};
