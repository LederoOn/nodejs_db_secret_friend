const express = require("express"), routes = require("./routes/routes.js")
const app = express();

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  res.header("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PATCH, DELETE");
  next();
});

app.use(express.json())

app.get("/friends", routes.getAll)
app.post("/friends", routes.newFriend)
app.get("/friends/:id", routes.getById)
app.patch("/friends/:id", routes.updateById)
app.delete("/friends/:id", routes.deleteById)
app.post("/friends/send", routes.sendMailById)

app.listen(3002)