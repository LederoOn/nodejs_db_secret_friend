const editJsonFile = require("edit-json-file");
const nodemailer = require("nodemailer")
let file = editJsonFile("./database/friends.json")

exports.addNew = (newFriend) => {
    const data = file.get("friends")

    if (newFriend.email && newFriend.name) {
        if (!data.find(obj => obj.email === newFriend.email)) {
            file.append("friends", {...newFriend, id: file.get("friends").length + 1})
            file.save()
            return newFriend
        };
    }

    return 400
};

exports.getFriend = (id=null) => {
    const data = file.get("friends")
    
    if (id) {
        return data.find(obj => obj.id === id)
    }

    return data
};

exports.updateFriend = (id, changes) => {
    const data = file.get("friends")
    if (data.find(obj => obj.email === changes.email)) {
        return 400
    }
    
    if (changes.name) {
        data[data.indexOf(data.find(obj => obj.id === id))] = {...data[data.indexOf(data.find(obj => obj.id === id))], name: changes.name}
    }

    if (changes.email) {
        data[data.indexOf(data.find(obj => obj.id === id))] = {...data[data.indexOf(data.find(obj => obj.id === id))], email: changes.email}
    }

    return data[data.indexOf(data.find(obj => obj.id === id))]
};

exports.deleteFriend = (id) => {
    const data = file.get("friends")
    if (data.find(obj => obj.id === id)) {
        data.splice(data.indexOf(data.find(obj => obj.id === id)), 1)
        
        file.set("friends", data)
        file.save()
        return data
    }

    return 400
};

exports.sendMailById = (target) => {
    console.log(target)

    async function main() {
    let testAccount = await nodemailer.createTestAccount();
    const transporter = nodemailer.createTransport({
        host: "smtp.ethereal.email",
        port: 587,
        secure: false, // true for 465, false for other ports
        auth: {
        user: testAccount.user, // generated ethereal user
        pass: testAccount.pass, // generated ethereal password
        },
    });

    let info = await transporter.sendMail({
        from: `"Your Secret Friend", <ruthe.greenholt79@ethereal.email>`,
        to: `${target.email}`, 
        subject: "You have a Secret Friend!",
        text: `Hi, your Secret Friend is ${target.name}! Make sure you give him or her a great present!`,
        html: "<b>Secret Friend</b>",
    });

    console.log("Message sent: %s", info.messageId);

    return ("Preview URL: %s", nodemailer.getTestMessageUrl(info));
    }
    return main().catch(console.error);
};